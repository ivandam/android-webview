package com.example.aplicacion1;

import android.annotation.SuppressLint;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import utils.PreferencesUtil;
import utils.Util;

public abstract class Abstract extends AppCompatActivity {
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        boolean kioskMode = PreferencesUtil.getPreferenceAsBoolean(PreferencesUtil.PROPERTY_KIOSK_MODE, this);
        enableKioskMode(kioskMode);

        hideUI();

}

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void enableKioskMode(boolean enabled) {

        try {

            DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getApplicationContext().getSystemService(Context.DEVICE_POLICY_SERVICE);
            enableAppOwner(devicePolicyManager, enabled);

            if (enabled) {

                if (devicePolicyManager.isLockTaskPermitted(this.getPackageName())) {

                    startLockTask();

                    Log.d(Abstract.class.getSimpleName(), "Started LockTask");

                } else {

                    Log.d(Abstract.class.getSimpleName(), "LockTask not permitted");
                }

            } else {

                stopLockTask();

                Log.d(Abstract.class.getSimpleName(), "Stopped LockTask");
            }

        } catch (Exception e) {

            Log.d(this.getClass().getSimpleName(), "Error enabling kiosk mode");
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void enableAppOwner(DevicePolicyManager devicePolicyManager, boolean enabled){

        ComponentName deviceAdmin = new ComponentName(this, MyAdmin.class);

        try {

            if(enabled) {

                if (devicePolicyManager.isDeviceOwnerApp(this.getPackageName())) {
                    //Runtime.getRuntime().exec("dpm set-device-owner es.snc.att_mgt_app/.kiosk.AdminReceiver");

                    devicePolicyManager.setLockTaskPackages(
                            deviceAdmin,
                            new String[]{
                                    this.getPackageName()
                            });

                    Log.d(Abstract.class.getSimpleName(), "Enabled device owner");
                }

            } else {

                if (devicePolicyManager.isDeviceOwnerApp(this.getPackageName())) {

                    devicePolicyManager.wipeData(DevicePolicyManager.WIPE_RESET_PROTECTION_DATA);
                    devicePolicyManager.clearPackagePersistentPreferredActivities(deviceAdmin, this.getPackageName());
                    devicePolicyManager.clearDeviceOwnerApp(this.getPackageName());
                    devicePolicyManager.removeActiveAdmin(deviceAdmin);
                }

                Log.d(Abstract.class.getSimpleName(), "Disabled device owner");
            }

        } catch (Exception e) {

            Log.d(this.getClass().getSimpleName(), "Error enabling / disabling owner");
        }
    }
    @Override
    public void onBackPressed() {
        hideUI();
    }

    private void hideUI(){
        Util.hideSystemUI(getWindow());
        Util.hideKeyboard(getWindow(), this);
        Util.hideActionBar(this);
    }

}
