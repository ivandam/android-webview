package com.example.aplicacion1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.format.Time;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.URL;
import java.util.Timer;

import utils.PreferencesUtil;
import utils.Util;

import static utils.PreferencesUtil.PROPERTY_KIOSK_MODE;
import static utils.PreferencesUtil.PROPERTY_UPDATE_AT_START;


public class MainActivity extends Abstract {
    WebView miwebview;
    Button boton1;
    Button boton2;
    boolean pulsadoboton1 = false;
    boolean pulsadoboton2 = false;
    public int counter;
    boolean kioskmode;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        kioskmode = loadKiosk();
        if (kioskmode) {
            hideUI();
        } else {
            showUI();
        }
        hideUI();
        WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifi.setWifiEnabled(true)) {
            miwebview.reload();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        miwebview = (WebView) findViewById(R.id.webvisual);
        boton1 = findViewById(R.id.button1);
        boton2 = findViewById(R.id.button2);
        final TextView counttime = findViewById(R.id.contador);

        new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                counttime.setText(String.valueOf(counter));
                counter++;
            }


            @Override
            public void onFinish() {
                counttime.setText("Finished");
            }
        }.start();
        final WebSettings ajustesVisorWeb = miwebview.getSettings();
        ajustesVisorWeb.setJavaScriptEnabled(true);
        ajustesVisorWeb.setDomStorageEnabled(true);
        ajustesVisorWeb.setAllowFileAccess(true);
        miwebview.setWebViewClient(new WebViewClient());
        miwebview.setWebChromeClient(new WebChromeClient());
        miwebview.getSettings().setBuiltInZoomControls(true);
        miwebview.loadUrl(loadUrl());
        miwebview.getSettings().setAppCacheEnabled(true);

        boton1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pulsadoboton1 = true;
            }
        });
        boton2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                pulsadoboton2 = true;
                if (counter <= 30) {

                    if (pulsadoboton1 && pulsadoboton2) {
                        Intent intent2 = new Intent(MainActivity.this, EscribirUrl.class);
                        startActivity(intent2);
                    }
                } else {
                    pulsadoboton2 = false;
                    pulsadoboton1 = false;
                }

            }
        });

//        final View view = (View) findViewById(android.R.id.content);
//        if (view != null) {
//            //"hides" back, home and return button on screen.
//            view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE |
//                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
//                    View.SYSTEM_UI_FLAG_IMMERSIVE |
//                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
//                    View.SYSTEM_UI_FLAG_FULLSCREEN);
//            view.setOnSystemUiVisibilityChangeListener
//                    (new View.OnSystemUiVisibilityChangeListener() {
//                        @Override
//                        public void onSystemUiVisibilityChange(int visibility) {
//                            // Note that system bars will only be "visible" if none of the
//                            // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
//                            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
//                                view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE |
//                                        View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
//                                        View.SYSTEM_UI_FLAG_IMMERSIVE |
//                                        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |
//                                        View.SYSTEM_UI_FLAG_FULLSCREEN);
//                            }
//                        }
//                    });
//        }


    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onResume() {
        super.onResume();
        kioskmode = loadKiosk();
        if (kioskmode) {
            hideUI();
        } else {
            showUI();
        }

        DevicePolicyManager mDevicePolicyManager = (DevicePolicyManager) getSystemService(
                Context.DEVICE_POLICY_SERVICE);
        ComponentName mAdminComponentName = new ComponentName(getApplicationContext(), MyAdmin.class);
        mDevicePolicyManager.setLockTaskPackages(mAdminComponentName, new String[]{getPackageName()});
        startLockTask();
    }

    @Override
    public void onPause() {
        super.onPause();
        kioskmode = loadKiosk();
        if (kioskmode) {
            hideUI();
        } else {
            showUI();
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void finish() {
        stopLockTask();
        super.finish();
    }


    protected boolean loadKiosk() {
        Boolean b = PreferencesUtil.getPreferenceAsBoolean(PROPERTY_KIOSK_MODE, this);
        return b;
    }

    public String loadUrl() {
        String u = PreferencesUtil.getPreferenceAsText(PROPERTY_UPDATE_AT_START, getApplicationContext());
        return u;
    }


    private void hideUI() {
        Util.hideSystemUI(getWindow());
        Util.hideKeyboard(getWindow(), this);
        Util.hideActionBar(this);
    }

    private void showUI() {
        Util.showSystemUI(getWindow());
    }

    //    Impedir que el botón Atrás cierre la aplicación
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (miwebview.canGoBack()) {
                        miwebview.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);

    }
}

