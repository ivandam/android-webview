package com.example.aplicacion1;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import utils.PreferencesUtil;
import utils.Util;

import static utils.PreferencesUtil.PROPERTY_KIOSK_MODE;

public class ResolucionPantalla extends AppCompatActivity {
    private Spinner miSpinner;
    String elemento;
    boolean kioskmode;
    Switch cambiarorden;
    Switch hide;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        boolean kioskmode;

        kioskmode = loadKiosk();
        if (kioskmode) {
            hideUI();
        } else {
            showUI();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resolucion_pantalla);
        Button cambiar = findViewById(R.id.cambiar);
        Button volver = findViewById(R.id.buttonvovlerrp);
        miSpinner = findViewById(R.id.spinner);
        cambiarorden = (Switch) findViewById(R.id.switch2);
        hide = (Switch) findViewById(R.id.switch3);


        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        ArrayList<String> elementos = new ArrayList<>();
        elementos.add(height + "," + width);
        elementos.add("480,854");
        elementos.add("540,960");
        elementos.add("576,1024");
        elementos.add("720,1280");
        elementos.add("768,1366");
        elementos.add("900,1600");
        elementos.add("1080,1920");
        elementos.add("1440,2048");

//        elementos.add("854,480");
//        elementos.add("960,540");
//        elementos.add("1024,576");
//        elementos.add("1280,720");
//        elementos.add("1366,768");
//        elementos.add("1600,900");
//        elementos.add("1920,1080");
//        elementos.add("2048,1440");


        ArrayAdapter adp = new ArrayAdapter(ResolucionPantalla.this, android.R.layout.simple_spinner_dropdown_item, elementos);
        miSpinner.setAdapter(adp);

        miSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                elemento = (String) miSpinner.getAdapter().getItem(position);

                if (position > 0) {
                    Toast.makeText(ResolucionPantalla.this, "Seleccionaste: " + elemento, Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cambiarorden.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                elementos.set(1, "854,480");
                elementos.set(2, "960,540");
                elementos.set(3, "1024,576");
                elementos.set(4, "1280,720");
                elementos.set(5, "1366,768");
                elementos.set(6, "1600,900");
                elementos.set(7, "1920,1080");
                elementos.set(8, "2048,1440");

            } else {
                elementos.set(1, "480,854");
                elementos.set(2, "540,960");
                elementos.set(3, "576,1024");
                elementos.set(4, "720,1280");
                elementos.set(5, "768,1366");
                elementos.set(6, "900,1600");
                elementos.set(7, "1080,1920");
                elementos.set(8, "1440,2048");


            }
        });
        hide.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                hideUI();
                PreferencesUtil.savePreferenceAsBoolean(PreferencesUtil.PROPERTY_KIOSK_MODE, hide.isChecked(), getApplicationContext());

            } else {
                showUI();
                PreferencesUtil.savePreferenceAsBoolean(PreferencesUtil.PROPERTY_KIOSK_MODE, hide.isChecked(), getApplicationContext());

            }
        });

        cambiar.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override

            public void onClick(View v) {


                Settings.Global.putString(getApplicationContext().getContentResolver(),
                        "display_size_forced", elemento
                );
                Toast.makeText(getBaseContext(), "Reinicia el dispositivo para cambiar la resolución", Toast.LENGTH_LONG).show();

                //  DisplayMetrics displayMetrics = new DisplayMetrics();
                //getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                //int height = displayMetrics.heightPixels;
                //int width = displayMetrics.widthPixels;
                //Toast.makeText(ResolucionPantalla.this, "height = "  +height + "\twidth = " + width, Toast.LENGTH_LONG).show();

            }
        });

        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent volver = new Intent(ResolucionPantalla.this, EscribirUrl.class);
                startActivity(volver);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        kioskmode = loadKiosk();

        if (kioskmode == true) {
            hideUI();

        } else {
            showUI();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        kioskmode = loadKiosk();
        if (kioskmode) {
            hideUI();
        } else {
            showUI();
        }


    }

    protected boolean loadKiosk() {
        Boolean b = PreferencesUtil.getPreferenceAsBoolean(PROPERTY_KIOSK_MODE, this);
        return b;
    }

    private void hideUI() {
        Util.hideSystemUI(getWindow());
        Util.hideKeyboard(getWindow(), this);
        Util.hideActionBar(this);
    }

    private void showUI() {
        Util.showSystemUI(getWindow());
    }


}




