package com.example.aplicacion1;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import utils.PreferencesUtil;
import utils.Util;

import static utils.PreferencesUtil.PROPERTY_UPDATE_AT_START;

public class EscribirUrl extends AppCompatActivity {

    EditText insertar;
    Button boton1;
    Button botonir;
    String url2;
    Button botonvolver;
    ImageButton resolucion;
    ImageButton ajustes;
    ImageButton wifi;
    Switch simpleSwitch;
    ImageButton launcher;
    boolean a;
    String b;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.insertaurl);
        boton1 = findViewById(R.id.button1);
        insertar = (EditText) findViewById(R.id.URL);
        botonir = findViewById(R.id.ir);
        botonvolver = findViewById(R.id.buttonvolv);
        ajustes = findViewById(R.id.ajustes);
        wifi = findViewById(R.id.wifi);
        resolucion = findViewById(R.id.buttonresolucion);
        simpleSwitch = (Switch) findViewById(R.id.switch1);
        launcher=findViewById(R.id.launcher);
        url2 = loadUrl().replace("http://", "");
        insertar.setText(url2);

        a = loadSettings();

        if (a == true) {
            hideUI();

        } else {
            showUI();
        }

launcher.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(Settings.ACTION_HOME_SETTINGS));
    }

});

        botonir.setOnClickListener(new View.OnClickListener() {

                                       @Override
                                       public void onClick(View view) {
                                           if (insertar.getText().toString().equals("")) {
                                               Toast.makeText(getBaseContext(), "ERROR : debes rellenar los campos", Toast.LENGTH_LONG).show();
                                           } else {
                                               url2 = insertar.getText().toString();
////                                               Intent launchIntent = new Intent(getPackageManager().getLaunchIntentForPackage("<com.android.chrome>"));
////                                               launchIntent.setData(Uri.parse(url2));
////                                               if (launchIntent != null) {
////                                                   startActivity(launchIntent);//null pointer check in case package name was not found
////                                                   Toast.makeText(getBaseContext(), "No encontrado", Toast.LENGTH_LONG).show();
//

                                               Intent intent = new Intent(EscribirUrl.this, MainActivity.class);
                                               intent.setPackage("com.android.chrome");
                                               hideUI();
                                               String b = "http://";
                                               PreferencesUtil.savePreferenceAsText(PROPERTY_UPDATE_AT_START, b + url2, getApplicationContext());
                                               startActivity(intent);
                                           }

                                       }
                                   }
        );


        botonvolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EscribirUrl.this, MainActivity.class);
                startActivity(intent);
            }
        });
        resolucion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EscribirUrl.this, ResolucionPantalla.class);
                startActivity(intent);
            }
        });
        wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            }
        });
        ajustes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        });

        simpleSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                hideUI();
                PreferencesUtil.savePreferenceAsBoolean(PreferencesUtil.PROPERTY_KIOSK_MODE, simpleSwitch.isChecked(), getApplicationContext());

            } else {
                showUI();
                PreferencesUtil.savePreferenceAsBoolean(PreferencesUtil.PROPERTY_KIOSK_MODE, simpleSwitch.isChecked(), getApplicationContext());

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        a = loadSettings();

        if (a == true) {
            hideUI();

        } else {
            showUI();
        }
    }


    private void hideUI() {
        Util.hideSystemUI(getWindow());
        Util.hideKeyboard(getWindow(), this);
        Util.hideActionBar(this);
    }


    private void showUI() {
        Util.showSystemUI(getWindow());
    }

    private boolean loadSettings() {
        boolean kioskMode = PreferencesUtil.getPreferenceAsBoolean(PreferencesUtil.PROPERTY_KIOSK_MODE, this);
        simpleSwitch.setChecked(kioskMode);
        return kioskMode;
    }

    public String loadUrl() {
        String u = PreferencesUtil.getPreferenceAsText(PROPERTY_UPDATE_AT_START, getApplicationContext());
        return u;
    }
}
