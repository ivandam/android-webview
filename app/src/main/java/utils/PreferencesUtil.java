package utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class PreferencesUtil {
    private static final String DEFAULT_CONFIG_FILE = "config.properties";
    private static final String sharedPrefFile = "com.example.aplicacion1.nueva";
    public static final String PROPERTY_UPDATE_AT_START = "UPDATE_AT_START";
    public static final String PROPERTY_KIOSK_MODE = "KIOSK_MODE";




    public static String getPreferenceAsText(String key, Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "emi.soincon.es");
    }

    public static void savePreferenceAsText(String preferenceName, String value, Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();

        preferencesEditor.putString(preferenceName, value);

        preferencesEditor.apply();
    }

    private static String getProperty(String key, Context context) throws IOException {

        Properties properties = new Properties();
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = assetManager.open(DEFAULT_CONFIG_FILE);

        properties.load(inputStream);

        return properties.getProperty(key);
    }
    public static Boolean getPreferenceAsBoolean(String key, Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE);

        return sharedPreferences.getBoolean(key, false);
    }
    public static Boolean  getPreferenceAsBoolean(String key, Context context, Boolean defValue) {

        SharedPreferences sharedPreferences = context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE);

        return sharedPreferences.getBoolean(key, defValue);
    }
    public static void savePreferenceAsBoolean(String preferenceName, Boolean value, Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();

        preferencesEditor.putBoolean(preferenceName, value);

        preferencesEditor.apply();
    }



}
