
### Installing

Be careful to follow theese steps as they are:

- For kiosk mode, be sure of putting testOnly attribute. Without it you may have to make a factory reset for being able to uninstall the app

- For install the app with adb: ./adb install -t <yourRouteToApk>

- Before you change the resolution you must add this command in terminal: adb shell pm grant com.example.aplicacion1 android.permission.WRITE_SECURE_SETTINGS

- Also, for enabling System Owner you should use this command in terminal: adb shell dpm set-device-owner com.example.aplicacion1/.MyAdmin

- If you need to remove the admin, you should use this command in terminal: adb shell dpm remove-active-admin com.example.aplicacion1/.MyAdmin

- For uninstall the app once you removed the admin: adb uninstall com.example.aplicacion1





